# Color-Scheme Monitor

Monitor the system-wide (freedesktop.org) color scheme
and adapt unaware applications to it!

There is a [system-wide color-scheme preference][freedesktop settings portal]
that lots of applications follow automatically,
but for various reasons some don't.

This package allows you to configure a set of scripts
that will run whenever the color scheme changes,
with allows you to adapt applications
to that color scheme.


## Related Projects

The [Night Theme Switcher][] GNOME extension complements this project
by automatically switching between the default and dark modes
depending on the time of day.
It can also run custom commands when switching,
which could replace the part of this project that monitors the color scheme.

[Lipstick on a Pig][] does pretty much the same as this project,
but I disagree with a bunch of their design decisions.
Choice of color schemes is a _very_ personal matter,
so I wanted to make those configurable instead of hard-coding them.
Also I dislike the name.
(This project's name is bland but then so am I.)


## Requirements

* [systemd][]
* [xdg-desktop-portal][]


## Install

### Arch Linux (btw)

Install [from the AUR][aur]

### Fedora

Install [from Copr][copr]:

	dnf copr enable mkhl/gnomish
	dnf install color-scheme-monitor

### From Source

More prerequisites:

* Rust, Cargo etc.
* Sed

Clone this project,
install it with

	PREFIX=$HOME/.local ./install

then enable and start the systemd service

	systemctl --user daemon-reload
	systemctl --user enable --now color-scheme-monitor.service


## Configure

Create a file `~/.config/color-scheme-monitor/config.toml`
and configure to your heart's content!

Check out `config/example.toml` for an example!

[aur]: https://aur.archlinux.org/packages/color-scheme-monitor
[copr]: https://copr.fedorainfracloud.org/coprs/mkhl/gnomish/
[freedesktop settings portal]: https://flatpak.github.io/xdg-desktop-portal/#gdbus-org.freedesktop.portal.Settings
[lipstick on a pig]: https://codeberg.org/small-tech/lipstick
[night theme switcher]: https://nightthemeswitcher.romainvigier.fr/

[systemd]: https://systemd.io
[xdg-desktop-portal]: https://github.com/flatpak/xdg-desktop-portal
