# Change Log

All notable changes to this project will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [0.1.3] - 2022-11-13
### Fixed
- Resolve HOME directory at runtime

## [0.1.2] - 2022-10-17
- Install to LIBEXECDIR and USERUNITDIR
- RPM packaging

## [0.1.1] - 2022-10-11
- Install to lib (instead of libexec)
- More output

## [0.1.0] - 2022-10-10
- Initial release

[Unreleased]: https://codeberg.org/mkhl/color-scheme-monitor/compare/v0.1.3...HEAD
[0.1.3]: https://codeberg.org/mkhl/color-scheme-monitor/compare/v0.1.2...v0.1.3
[0.1.2]: https://codeberg.org/mkhl/color-scheme-monitor/compare/v0.1.1...v0.1.2
[0.1.1]: https://codeberg.org/mkhl/color-scheme-monitor/compare/v0.1.0...v0.1.1
[0.1.0]: https://codeberg.org/mkhl/color-scheme-monitor/releases/tag/v0.1.0
