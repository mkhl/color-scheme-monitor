Name:           {{{ git_dir_name }}}
Version:        {{{ git_dir_version }}}
Release:        1%{?dist}
Summary:        Monitor the system-wide color scheme and adapt unaware applications to it

License:        0BSD
URL:            https://codeberg.org/mkhl/%{name}
VCS:            {{{ git_dir_vcs }}}
Source:         {{{ git_dir_pack }}}

BuildRequires:  systemd-rpm-macros
BuildRequires:  cargo
BuildRequires:  sed
Requires:       dbus
Requires:       systemd
Requires:       xdg-desktop-portal

%description
This package allows you to configure a set of scripts
that will run whenever the color scheme changes,
which allows you to adapt applications to that color scheme.

%define _debugsource_template %{nil}

%prep
{{{ git_dir_setup_macro }}}
cargo fetch --locked --target "%{_arch}-unknown-linux-gnu"

%build
export RUSTUP_TOOLCHAIN=stable
export CARGO_TARGET_DIR=target
cargo build --frozen --release --all-features

%install
BUILD=no DESTDIR="%{buildroot}" PREFIX="%{_prefix}" LIBEXECDIR="%{_libexecdir}" USERUNITDIR="%{_userunitdir}" ./install

%if %{with check}
%check
cargo test --frozen --all-features
%endif

%files
%license LICENSE
%doc README.md
%doc config/example.toml
%{_libexecdir}/%{name}
%{_userunitdir}/%{name}.service

%changelog
* Mon Oct 17 2022 Martin Kühl <martin.kuehl@posteo.net> - 0.1.2-1
- Initial package
