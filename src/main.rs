mod changes;
mod monitor;
mod result;

use futures::executor::block_on;

use result::Result;

fn main() -> Result<()> {
    let args: Vec<String> = std::env::args().skip(1).collect();
    match &args[..] {
        [] => block_on(monitor::run()),
        [scheme, names @ ..] => changes::apply_color_scheme(scheme, names),
    }
}
