use std::error::Error;

pub(crate) type Result<T> = core::result::Result<T, Box<dyn Error>>;
