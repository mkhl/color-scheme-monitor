use futures::stream::StreamExt;
use zbus::{dbus_proxy, zvariant::Value, Connection};

use crate::{changes, result::Result};

#[dbus_proxy(
    interface = "org.freedesktop.portal.Settings",
    default_service = "org.freedesktop.portal.Desktop",
    default_path = "/org/freedesktop/portal/desktop"
)]
trait Settings {
    #[dbus_proxy(signal)]
    fn setting_changed(&self, namespace: &str, name: &str, value: Value<'_>) -> Result<()>;
}

pub(crate) async fn run() -> Result<()> {
    let conn = Connection::session().await?;
    let settings = SettingsProxy::new(&conn).await?;
    let mut stream = settings.receive_setting_changed().await?;
    eprintln!("listening...");
    while let Some(signal) = stream.next().await {
        let args = signal.args()?;
        if args.namespace != "org.freedesktop.appearance" || args.name != "color-scheme" {
            continue;
        }
        let value = u32::try_from(args.value())?;
        let scheme = match value {
            1 => "dark",
            2 => "light",
            _ => "default",
        };
        changes::apply_color_scheme(scheme, &[])?;
    }
    Ok(())
}
