use std::{
    env, fs,
    path::PathBuf,
    process::{Command, Stdio},
};

use serde::Deserialize;

use crate::result::Result;

#[derive(Deserialize, Debug, Default)]
struct Config {
    changes: Vec<Change>,
}

impl Config {
    fn changes_named(self: &Config, names: &[String]) -> Vec<&Change> {
        self.changes
            .iter()
            .filter(|change| names.is_empty() || names.contains(&change.name))
            .collect()
    }
}

#[derive(Deserialize, Debug)]
struct Change {
    name: String,
    files: Vec<PathBuf>,
    change: String,
    value: Value,
}

#[derive(Deserialize, Debug)]
struct Value {
    dark: String,
    light: String,
    default: String,
}

impl Value {
    fn for_scheme(self: &Value, scheme: &str) -> &String {
        match scheme {
            "dark" => &self.dark,
            "light" => &self.light,
            _ => &self.default,
        }
    }
}

fn config() -> Result<Config> {
    let dirs = xdg::BaseDirectories::with_prefix(env!("CARGO_PKG_NAME"))?;
    let path = dirs.place_config_file("config.toml")?;
    if !path.exists() {
        return Ok(Config::default());
    }
    let data = fs::read(path)?;
    let changes = toml::from_slice(&data)?;
    Ok(changes)
}

fn apply(changes: Vec<&Change>, scheme: &str) -> Result<()> {
    for change in changes {
        let name = &change.name;
        eprintln!("changing: '{name}'");
        let value = change.value.for_scheme(scheme);
        for file in &change.files {
            let change = &change.change;
            let path = file.display();
            eprintln!("applying: '{change}', VALUE='{value}', FILE='{path}'");
            Command::new("sh")
                .arg("-c")
                .arg(change)
                .env("FILE", file)
                .env("VALUE", &value)
                .current_dir(env::var("HOME")?)
                .stdin(Stdio::null())
                .status()
                .map(|_| ())?
        }
    }
    Ok(())
}

pub(crate) fn apply_color_scheme(scheme: &str, names: &[String]) -> Result<()> {
    let scheme = scheme.strip_prefix("prefer-").unwrap_or(scheme);
    eprintln!("color-scheme: {scheme}");
    let config = config()?;
    let changes = config.changes_named(names);
    apply(changes, scheme)
}

